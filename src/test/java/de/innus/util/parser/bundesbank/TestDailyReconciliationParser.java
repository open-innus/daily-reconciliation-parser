/*
 * Copyright 2021 innus GmbH <info@innus.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense,  and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESSFOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package de.innus.util.parser.bundesbank;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.net.URI;
import java.util.Optional;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestDailyReconciliationParser {

  public TestDailyReconciliationParser() {
    super();
  }

  @Test
  public void givenValidFileShouldParse() throws Exception {
    final URI testFileLocation =
        TestDailyReconciliationParser.class
            .getResource("/data/drc-test-file.dat").toURI();
    final File testFile = new File(testFileLocation);
    try (
        final InputStream inputStream = new FileInputStream(testFile);
        final BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream)
    ) {
      final DailyReconciliationParser parser =
          DailyReconciliationParser.create(bufferedInputStream);

      final Optional<DailyReconciliationHeader> optionalHeader = parser.header();
      Assertions.assertTrue(optionalHeader.isPresent());

      try {
        while (parser.nextBody()) {
          Assertions.assertTrue(parser.body().isPresent());
        }

        final Optional<DailyReconciliationTrailer> optionalTrailer = parser.trailer();
        Assertions.assertTrue(optionalTrailer.isPresent());
        Assertions.assertEquals(3, optionalTrailer.get().getNumberOfRecords());
      } catch (final Exception ex) {
        Assertions.fail(ex);
      }

    } catch (final Exception ex) {
      Assertions.fail(ex);
    }
  }

  @Test
  public void givenValidFileShouldTranslate() throws Exception {
    final URI testFileLocation =
        TestDailyReconciliationParser.class
            .getResource("/data/drc-test-file.dat").toURI();
    final File testFile = new File(testFileLocation);
    try (
        final InputStream inputStream = new FileInputStream(testFile);
        final BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream)
    ) {
      final DailyReconciliationParser parser =
          DailyReconciliationParser.create(bufferedInputStream);

      final Optional<String> optionalTranslated = parser.translate();
      Assertions.assertTrue(optionalTranslated.isPresent());
      System.out.println(optionalTranslated.get());

    } catch (final Exception ex) {
      Assertions.fail(ex);
    }
  }
}
