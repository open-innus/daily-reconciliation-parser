/*
 * Copyright 2021 innus GmbH <info@innus.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense,  and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESSFOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package de.innus.util.parser.bundesbank;

import java.time.LocalDate;
import java.time.LocalDateTime;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@Getter
@Setter
@ToString
public class DailyReconciliationHeader {
  public static final Integer RECORD_TYPE_LENGTH = 4;
  public static final Integer SERVICE_IDENTIFIER_LENGTH = 3;
  public static final Integer FILE_TYPE_LENGTH = 3;
  public static final Integer SENDING_INSTITUTION_LENGTH = 8;
  public static final Integer FILE_REFERENCE_LENGTH = 16;
  public static final Integer DATE_TIME_LENGTH = 12;
  public static final Integer TEST_CODE_LENGTH = 1;
  public static final Integer RECEIVING_INSTITUTION_LENGTH = 11;
  public static final Integer BUSINESS_DATE_LENGTH = 6;

  public enum RecordType {
    HDRC(64),
    HDRQ(64),
    HDRD(64);

    private final Integer length;

    RecordType(final Integer length) {
      this.length = length;
    }

    public Integer length() {
      return this.length;
    }
  }
  private RecordType recordType;
  private String serviceIdentifier;
  private String fileType;
  private String sendingInstitution;
  private String fileReference;
  private LocalDateTime dateTime;
  private Boolean test;
  private String receivingInstitution;
  private LocalDate businessDate;
}
