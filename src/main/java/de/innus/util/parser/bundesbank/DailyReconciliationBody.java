/*
 * Copyright 2021 innus GmbH <info@innus.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense,  and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESSFOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package de.innus.util.parser.bundesbank;

import java.math.BigDecimal;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@Getter
@Setter
@ToString
public class DailyReconciliationBody {
  public static final Integer RECORD_TYPE_LENGTH = 4;
  public static final Integer BULK_REFERENCE_LENGTH = 35;
  public static final Integer NUMBER_OF_ITEMS_LENGTH = 8;
  public static final Integer VALUE_LENGTH = 18;
  public static final Integer CYCLE_NUMBER_LENGTH = 2;

  public enum RecordType {
    DTSB(93),
    DRSB(93),
    DRIB(93),
    DTRB(67),
    DRCB(67),
    DRRB(67),
    DROB(67),
    DDSB(93),
    DFSB(93),
    DVSB(93),
    DJSB(93),
    DDRB(67),
    DFDB(67),
    DVDB(67),
    DJRB(67),
    DCRB(67),
    DCSB(93),
    QCNS(93),
    QMPS(93),
    QROS(93),
    QSRS(93),
    QCNR(67),
    QMPR(67),
    QROR(67),
    QSRR(67);

    private final int length;

    RecordType(final int length) {
      this.length = length;
    }

    public int length() {
      return length;
    }
  }
  private RecordType recordType;
  private String bulkReference;
  private Integer processedOrReceivedItems;
  private Integer rejectedItems;
  private BigDecimal monetaryValueProcessedOrReceived;
  private BigDecimal monetaryValueRejected;
  private Integer cycleNumber;
}
