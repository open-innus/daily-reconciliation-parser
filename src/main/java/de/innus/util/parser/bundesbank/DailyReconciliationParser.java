/*
 * Copyright 2021 innus GmbH <info@innus.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense,  and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESSFOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package de.innus.util.parser.bundesbank;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.charset.Charset;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;

public class DailyReconciliationParser {
  private static final Charset EBCDIC_CODE_PAGE = Charset.forName("Cp1047");
  private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("yyMMddHHmmss");
  private static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("yyMMdd");

  private final AtomicInteger currentPosition = new AtomicInteger(0);
  private final BufferedInputStream inputStream;
  private final Integer actualSize;

  private DailyReconciliationParser(final BufferedInputStream inputStream) throws IOException {
    super();
    this.inputStream = inputStream;
    this.actualSize = this.inputStream.available();
  }

  public static DailyReconciliationParser create(final BufferedInputStream inputStream) throws IOException {
    return new DailyReconciliationParser(inputStream);
  }

  public Optional<DailyReconciliationHeader> header() throws IOException {
    if (this.currentPosition.get() > 0) {
      return Optional.empty();
    }

    final byte[] recordTypeBytes = new byte[DailyReconciliationHeader.RECORD_TYPE_LENGTH];
    this.inputStream.read(recordTypeBytes, 0, recordTypeBytes.length);

    final DailyReconciliationHeader.RecordType recordType;
    try {
      recordType =
          DailyReconciliationHeader.RecordType.valueOf(new String(recordTypeBytes, EBCDIC_CODE_PAGE));
    } catch (final Exception ex) {
      return Optional.empty();
    }

    final DailyReconciliationHeader header = new DailyReconciliationHeader();
    header.setRecordType(recordType);

    final byte[] headerBytes = new byte[recordType.length() - DailyReconciliationHeader.RECORD_TYPE_LENGTH];
    this.inputStream.read(headerBytes, 0, headerBytes.length);

    final AtomicInteger headerPosition = new AtomicInteger(0);
    header.setServiceIdentifier(
        new String(
            headerBytes,
            headerPosition.get(),
            DailyReconciliationHeader.SERVICE_IDENTIFIER_LENGTH,
            EBCDIC_CODE_PAGE
        )
    );

    header.setFileType(
        new String(
            headerBytes,
            headerPosition.addAndGet(DailyReconciliationHeader.SERVICE_IDENTIFIER_LENGTH),
            DailyReconciliationHeader.FILE_TYPE_LENGTH,
            EBCDIC_CODE_PAGE
        )
    );

    header.setSendingInstitution(
        new String(
            headerBytes,
            headerPosition.addAndGet(DailyReconciliationHeader.FILE_TYPE_LENGTH),
            DailyReconciliationHeader.SENDING_INSTITUTION_LENGTH,
            EBCDIC_CODE_PAGE
        )
    );

    header.setFileReference(
        new String(
            headerBytes,
            headerPosition.addAndGet(DailyReconciliationHeader.SENDING_INSTITUTION_LENGTH),
            DailyReconciliationHeader.FILE_REFERENCE_LENGTH,
            EBCDIC_CODE_PAGE
        )
    );

    header.setDateTime(
        LocalDateTime.from(
            DATE_TIME_FORMATTER.parse(
                new String(
                    headerBytes,
                    headerPosition.addAndGet(DailyReconciliationHeader.FILE_REFERENCE_LENGTH),
                    DailyReconciliationHeader.DATE_TIME_LENGTH,
                    EBCDIC_CODE_PAGE
                )
            )
        )
    );

    final String testCode = new String(
        headerBytes,
        headerPosition.addAndGet(DailyReconciliationHeader.DATE_TIME_LENGTH),
        DailyReconciliationHeader.TEST_CODE_LENGTH,
        EBCDIC_CODE_PAGE
    );

    header.setTest(testCode.equalsIgnoreCase("T"));

    header.setReceivingInstitution(
        new String(
            headerBytes,
            headerPosition.addAndGet(DailyReconciliationHeader.TEST_CODE_LENGTH),
            DailyReconciliationHeader.RECEIVING_INSTITUTION_LENGTH,
            EBCDIC_CODE_PAGE
        )
    );

    header.setBusinessDate(
        LocalDate.from(
            DATE_FORMATTER.parse(
                new String(
                    headerBytes,
                    headerPosition.addAndGet(DailyReconciliationHeader.RECEIVING_INSTITUTION_LENGTH),
                    DailyReconciliationHeader.BUSINESS_DATE_LENGTH,
                    EBCDIC_CODE_PAGE
                )
            )
        )
    );

    this.currentPosition.addAndGet(header.getRecordType().length());

    return Optional.of(header);
  }

  public Boolean nextBody() {
    return this.currentPosition.get() >= 64 && (this.actualSize - this.currentPosition.get()) > 10;
  }

  public Optional<DailyReconciliationBody> body() throws IOException {
    if (this.currentPosition.get() < 64 || (this.actualSize - this.currentPosition.get()) <= 10) {
      return Optional.empty();
    }

    final byte[] recordTypeBytes = new byte[DailyReconciliationBody.RECORD_TYPE_LENGTH];
    this.inputStream.read(recordTypeBytes, 0, recordTypeBytes.length);

    final DailyReconciliationBody.RecordType recordType;
    final String temp = new String(recordTypeBytes, EBCDIC_CODE_PAGE);
    try {
      recordType =
          DailyReconciliationBody.RecordType.valueOf(new String(recordTypeBytes, EBCDIC_CODE_PAGE));
    } catch (final Exception ex) {
      throw new IllegalArgumentException(temp);
    }

    final Boolean isSubmission = this.isSubmission(recordType);
    final DailyReconciliationBody body = new DailyReconciliationBody();
    body.setRecordType(recordType);

    final byte[] bodyBytes = new byte[recordType.length() - DailyReconciliationBody.RECORD_TYPE_LENGTH];
    this.inputStream.read(bodyBytes, 0, bodyBytes.length);

    final AtomicInteger bodyPosition = new AtomicInteger(0);
    body.setBulkReference(
        new String(
            bodyBytes,
            bodyPosition.get(),
            DailyReconciliationBody.BULK_REFERENCE_LENGTH,
            EBCDIC_CODE_PAGE
        ).trim()
    );

    body.setProcessedOrReceivedItems(
        Integer.valueOf(
            new String(
                bodyBytes,
                bodyPosition.addAndGet(DailyReconciliationBody.BULK_REFERENCE_LENGTH),
                DailyReconciliationBody.NUMBER_OF_ITEMS_LENGTH,
                EBCDIC_CODE_PAGE
            )
        )
    );

    if (isSubmission) {
      body.setRejectedItems(
          Integer.valueOf(
              new String(
                  bodyBytes,
                  bodyPosition.addAndGet(DailyReconciliationBody.NUMBER_OF_ITEMS_LENGTH),
                  DailyReconciliationBody.NUMBER_OF_ITEMS_LENGTH,
                  EBCDIC_CODE_PAGE
              )
          )
      );
    }

    body.setMonetaryValueProcessedOrReceived(
        new BigDecimal(
            new String(
                bodyBytes,
                bodyPosition.addAndGet(DailyReconciliationBody.NUMBER_OF_ITEMS_LENGTH),
                DailyReconciliationBody.VALUE_LENGTH,
                EBCDIC_CODE_PAGE
            )
        )
    );

    if (isSubmission) {
      body.setMonetaryValueRejected(
          new BigDecimal(
              new String(
                  bodyBytes,
                  bodyPosition.addAndGet(DailyReconciliationBody.VALUE_LENGTH),
                  DailyReconciliationBody.VALUE_LENGTH,
                  EBCDIC_CODE_PAGE
              )
          )
      );
    }

    body.setCycleNumber(
        Integer.valueOf(
            new String(
                bodyBytes,
                bodyPosition.addAndGet(DailyReconciliationBody.VALUE_LENGTH),
                DailyReconciliationBody.CYCLE_NUMBER_LENGTH,
                EBCDIC_CODE_PAGE
            )
        )
    );

    this.currentPosition.addAndGet(body.getRecordType().length());

    return Optional.of(body);
  }

  public Optional<DailyReconciliationTrailer> trailer() throws IOException {
    if ((this.actualSize - this.currentPosition.get()) > 10) {
      return Optional.empty();
    }

    final byte[] recordTypeBytes = new byte[DailyReconciliationTrailer.RECORD_TYPE_LENGTH];
    this.inputStream.read(recordTypeBytes, 0, recordTypeBytes.length);

    final DailyReconciliationTrailer.RecordType recordType;
    try {
      recordType =
          DailyReconciliationTrailer.RecordType.valueOf(new String(recordTypeBytes, EBCDIC_CODE_PAGE));
    } catch (final Exception ex) {
      return Optional.empty();
    }

    final DailyReconciliationTrailer trailer = new DailyReconciliationTrailer();
    trailer.setRecordType(recordType);

    final byte[] trailerBytes = new byte[recordType.length() - DailyReconciliationTrailer.RECORD_TYPE_LENGTH];
    this.inputStream.read(trailerBytes, 0, trailerBytes.length);

    final AtomicInteger trailerPosition = new AtomicInteger(0);
    trailer.setNumberOfRecords(
        Integer.valueOf(
            new String(
                trailerBytes,
                trailerPosition.get(),
                DailyReconciliationTrailer.NUMBER_OF_RECORDS_LENGTH,
                EBCDIC_CODE_PAGE
            )
        )
    );

    this.currentPosition.addAndGet(trailer.getRecordType().length());

    return Optional.of(trailer);
  }

  public Optional<String> translate() {
    try {
      final byte[] translationBytes = new byte[this.actualSize];
      this.inputStream.read(translationBytes, 0, translationBytes.length);
      return Optional.of(new String(translationBytes, EBCDIC_CODE_PAGE));
    } catch (final Exception ex) {
      return Optional.empty();
    }
  }

  private Boolean isSubmission(final DailyReconciliationBody.RecordType recordType) {
    switch (recordType) {
      case DCSB:
      case DRIB:
      case DRSB:
      case DTSB:
        return true;
      default:
        return false;
    }
  }
}
