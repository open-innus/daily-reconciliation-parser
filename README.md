## Summary
The Deutsche Bundesbank is sending proprietary files for daily reconciliation
purposes at the end of every business day. These files are 
[EBCDIC](https://en.wikipedia.org/wiki/EBCDIC) encoded and follow well-defined 
schemas which can be found here:

- [SCT/SCL technical specifications](https://www.bundesbank.de/resource/blob/793254/b508b3f1adac56732c04c814df1032dd/mL/technische-spezifikationen-sct-scl-version-1-1-data.pdf)
- [SDD/SCL technical specifications](https://www.bundesbank.de/resource/blob/797044/a0c73afc28796a44f86766f0d6b67089/mL/technische-spezifikatione-sdd-scl-version-1-0-data.pdf)

This library is designed to parse these file fast and memory efficient, providing 
simple abstract domain objects for easy consumption in any Java application.

## Usage

### Instantiation
The centerpiece of this library is the class `DailyReconciliationParser`. 
To create an instance simply call the `create` method, for example:
```
try (
    final InputStream inputStream = new FileInputStream(testFile);
    final BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream)
) {
  final DailyReconciliationParser parser =
      DailyReconciliationParser.create(bufferedInputStream);
} catch (final Exception ex) {
  ex.printStackTrace();
}
```

### Processing
After obtaining a parser the different parts of a file can be processed sequentially.
First an instance of `DailyReconciliationHeader` can be obtained:
```
parser.header().ifPresent(header -> {
  ...
});
````
Then all `DailyReconciliationBody`'s can be processed:
```
while (parser.nextBody()) {
  parser.body().ifPresent(body -> {
    ...
  });
}
```
The last step is to obtain an instance of `DailyReconciliationTrailer`:
```
parser.trailer().ifPresent(trailer -> {
  ...
});
```

### Simple parsing
It is possible to translate the complete file content into an ASCII encoded version:
```
parser.translate().ifPresent(translated -> {
  ...
});
```

## Access to Binaries
innus publishes release versions to [Maven Central](https://search.maven.org/), 
just add the dependencies to your project's POM:

### Maven
```
<dependency>
    <groupId>de.innus.util</groupId>
    <artifactId>daily-reconciliation-parser</artifactId>
    <version>1.0.0</version>
</dependency>
```

### Gradle
```
implementation 'de.innus.util:daily-reconciliation-parser:1.0.0'
```

## License
This library ist released under the [MIT License](https://mit-license.org/).